# the dotfiles

Well, here we go again.

These are my current dotfiles which are used on my system.

## installation

To install these, you need to install some software first:

- `stow` for installing the dotfiles
- `st` the terminal
	- I've installe some patches like `invert` or `font2`, but it's up to preference
- `zsh` the prompt
	- `oh-my-zsh` for the plugins and shit
	- `starship` for a wonderful prompt
- `bspwm` a tiling window manager
	- `sxhkd` the hotkey deamon
	- `redshift` so I can sleep after working at night
		- you may need to edit the `-l` arg to match it to your own destination
	- `picon` which replaced `compton` for some reason
	- `polybar` for the statusbar
	- `feh` for the background
- `nvim` the one and only editor
	- BEHOLD: unless you are using colemak too, you need to remove the line to install `colemak.vim` in `install-nvim-plugins.sh` and edit the mappings for `<esc>` in `init.vim`
	- `ag` aka `the silver searcher` for `CtrlP`

