#!/bin/bash

cd nvim/.config/nvim/bundle

git clone https://github.com/jooize/vim-colemak
git clone https://github.com/tpope/vim-fugitive
git clone https://github.com/ctrlpvim/ctrlp.vim
git clone https://github.com/chriskempson/base16-vim
git clone https://github.com/vim-scripts/brainfuck-syntax
git clone https://github.com/itchyny/lightline.vim
git clone https://github.com/godlygeek/tabular
git clone https://github.com/tpope/vim-endwise
git clone https://github.com/farmergreg/vim-lastplace
git clone https://github.com/fmoralesc/vim-pad
git clone https://github.com/cespare/vim-toml
git clone https://github.com/insanum/votl
git clone https://github.com/arcticicestudio/nord-vim

